# soal-shift-sisop-modul-1-ita07-2022 
Kelompok ITA07
1. Maulanal Fatihil A. M.   5027201031
2. Cherylene Trevina        5027201033
3. M. Hilmi Azis            5027201049

# Daftar Isi
* [Nomor 1](#nomor-1)
    [Penyelesaian Nomor 1](#penyelesaian-nomor-2)
    * [1a](#1a-pembuatan-file-register)
    * [1b](#1b-pembuatan-kriteria-password)
    * [1c](#1c-update-login-dan-register-pada-file-log.txt)
    * [1d](#1d-opsi-setelah-login)
* [Penyelesaian Nomor 2](#penyelesaian-nomor-1)
* [Nomor 2](#nomor-2)
* [Penyelesaian Nomor 2](#penyelesaian-nomor-2)
    * [2a](#2a-pembuatan-folder)
    * [2b](#2b-rata-rata-request-per-jam)
    * [2c](#2c-ip-yang-paling-banyak-muncul)
    * [2d](#2d-request-oleh-user-agent-curl)
    * [2e](#2e-ip-pada-tanggal-22-januari-jam-2-pagi)
        * [Output nomor 2](#output-nomor-2)
* [Nomor 3](#nomor-3)
* [Penyelesaian Nomor 3](#penyelesaian-nomor-3)
    * [3a](#3a-masukkan-metrics-ke-file-log)
    * [3b](#3b-metrics-berjalan-otomatis-setiap-menit)
    * [3c](#3c-membuat-agregasi-file-log-ke-satuan-jam)
    * [3d](#3d-file-log-hanya-bisa-diakses-oleh-user)
--- 

## Nomor 1
 1.	Pada suatu hari, Han dan teman-temannya diberikan tugas untuk mencari foto. Namun, karena laptop teman-temannya rusak ternyata tidak bisa dipakai karena rusak, Han dengan senang hati memperbolehkan teman-temannya untuk meminjam laptopnya. Untuk mempermudah pekerjaan mereka, Han membuat sebuah program.<br>
    a.	Han membuat sistem register pada script register.sh dan setiap user yang berhasil didaftarkan disimpan di dalam file ./users/user.txt. Han juga membuat sistem login yang dibuat di script main.sh <br>
    b.	Demi menjaga keamanan, input password pada login dan register harus tertutup/hidden dan password yang didaftarkan memiliki kriteria sebagai berikut <br>
        i.	Minimal 8 karakter <br>
        ii.	Memiliki minimal 1 huruf kapital dan 1 huruf kecil<br>
        iii.	Alphanumeric<br>
        iv.	Tidak boleh sama dengan username<br>
    c.	Setiap percobaan login dan register akan tercatat pada log.txt dengan format : MM/DD/YY hh:mm:ss MESSAGE. Message pada log akan berbeda tergantung aksi yang dilakukan user.<br>
        i.	Ketika mencoba register dengan username yang sudah terdaftar, maka message pada log adalah REGISTER: ERROR User already exists<br>
        ii.	Ketika percobaan register berhasil, maka message pada log adalah REGISTER: INFO User USERNAME registered successfully<br>
        iii.	Ketika user mencoba login namun passwordnya salah, maka message pada log adalah LOGIN: ERROR Failed login attempt on user USERNAME<br>
        iv.	Ketika user berhasil login, maka message pada log adalah LOGIN: INFO User USERNAME logged in<br>
    d.	Setelah login, user dapat mengetikkan 2 command dengan dokumentasi sebagai berikut :<br>
        i.	dl N ( N = Jumlah gambar yang akan didownload)<br>
        Untuk mendownload gambar dari https://loremflickr.com/320/240 dengan jumlah sesuai dengan yang diinputkan oleh user. Hasil download akan dimasukkan ke dalam folder dengan format nama YYYY-MM-DD_USERNAME. Gambar-gambar yang didownload juga memiliki format nama PIC_XX, dengan nomor yang berurutan (contoh : PIC_01, PIC_02, dst. ).  Setelah berhasil didownload semua, folder akan otomatis di zip dengan format nama yang sama dengan folder dan dipassword sesuai dengan password user tersebut. Apabila sudah terdapat file zip dengan nama yang sama, maka file zip yang sudah ada di unzip terlebih dahulu, barulah mulai ditambahkan gambar yang baru, kemudian folder di zip kembali dengan password sesuai dengan user.<br>
        ii.	att
        Menghitung jumlah percobaan login baik yang berhasil maupun tidak dari user yang sedang login saat ini.<br>


## Penyelesaian Nomor 1
Script dimulai dengan 
```sh
#!/bin/bash
``` 
syntax diatas digunakan untuk memberi tahu kernel linux guna mengeksekusi path yang disertakan dalam program (bash).
### 1a pembuatan file register
Soal meminta praktikan untuk membuat file register.sh yang memiliki fungsi untuk mendaftarkan user, yang mana kemudian praktikan juga perlu membuat file user.txt di folder users yang akan digunakan sebagai tempat menyimpan username dan password, dan kemudian membuat file main.sh yang akan digunakan untuk login user yang sudah terdaftar.

- Sistem register

    ```
    if [ ! -d log.txt ]; then
        touch log.txt
    fi

    if [ ! -d ./users ]; then
        mkdir ./users
    fi

    if [ ! -d ./users/user.txt ]; then
        touch ./users/user.txt
    fi

    time="$(date +'%D %H:%M:%S')"
    ```
- ``-d`` memiliki fungsi untuk pengecekan file/directory 
- variabel time berisi tanggal,jam,menit, dan detik yang akan berguna untuk mempermudah pengisian laporan pada log.txt

Akan melakukan pengecekan terkait folder log.txt dan folder users yang di dalamnya terdapat file user.txt sudah terbuat atau belum, apabila belum terbuat maka akan langsung proses membuat folder tersebut.




### 1b pembuatan kriteria password
pada tahap register, password yang diisikan harus memiliki beberapa kriteria yang telah disebutkan, antara lain:
i. minimal 8 karakter : 
```shell
[ `expr length "$password"` -lt 8 ] 
```
pada bagian expr length password, akan berfungsi untuk menghitung jumlah huruf pada password, sedangkan -lt 8 : Less Then berfungsi untuk mengecek apakah password yang telah dihitung tadi kurang dari 8.

ii. memiliki minimal 1 huruf kapital dan 1 huruf kecil: 

```shell
if [[ $password = ${password^^} ]] ; then 
   echo "Password minimal 1 huruf kecil" 
```

 pada bagian ini, password akan disamakan dengan password yang telah dijadikan huruf kapital semuanya. apabila terjadi kesamaan maka akan menampilkan pesan "Password minimal 1 huruf kecil".
    

```shell
elif [[ $password = ${password,,} ]] ; then 
   echo "Password minimal 1 huruf kecil" 
fi
```
 pada bagian ini, berkebalikan dengan sebelumnya jadi password akan disamakan dengan password yang telah dijadikan huruf kecil semuanya. jikalau sama maka akan menampilkan pesan "Password minimal 1 huruf kapital".

iii. Alphanumeric
```shell
if ! [[ "$password" =~ [0-9] ]] ; then 
   echo "Password harus ada angkanya"
elif
```
pada bagian ini, password akan di cek menggunakan "=~", apakah password tersebut tidak terdapat angka 0-9 . dan apabila tidak ada maka akan menampilkan pesan "password harus ada angkanya"

iv. tidak boleh sama dengan username.
```shell
elif
[ $username == $password ]; then 
     echo "Password tidak boleh sama dengan username"
```
pada bagian ini, akan di cek apakah antara password dan usernamenya sama atau tidak.
jika iya, maka akan muncul pesan "Password tidak boleh sama dengan username".

```shell
   echo "$username $password" >> ./users/user.txt && echo "$username berhasil ditambahkan" &&  echo "$time REGISTER: INFO User $username registered successfully" >> log.txt
```
setelah berhasil terdaftar, akan ada pesan  "$username berhasil ditambahkan", username dan passwordnya akan disimpan di file user.txt, dan akan ada laporan bahwa ada user yang telah terdaftar pada log.txt <br>
![Output result](img/1b.jpg)



## 1c update login dan register pada file log.txt
setiap terjadinya percobaan login dan register akan disimpan pada file log.txt

i. jika username yang dimasukan sudah ada yang terdaftar maka akan ditampilkan pesan kalau user telah terdaftar lalu laporannya akan dimasukan ke log.txt

 ```shell
 if grep -q "$username" ./users/user.txt ; then
   echo "User $username sudah ada"
   echo "$time REGISTER: ERROR $username already exists" >> log.txt 
```
- ``grep -q`` memiliki fungsi untuk mencari kata/kalimat yang ditunjuk pada sebuah file

![Output result](img/1c1.jpg)

ii. ketika percobaan register berhasil, maka akan ditambahkan laporan "username register successfully" pada log.txt
![Output result](img/1c2.jpg)

iii. ketika user mencoba untuk login kemudian password yang diisikan salah maka akan dilaporkan bahwa user tersebut gagal login
```shell
 else
     echo "Login tidak berhasil"
     echo "$time LOGIN: ERROR Failed login attempt on user $username"  >> log.txt
     exit 1
 fi
```
![Output result](img/1c3.jpg)

iv. ketika user berhasil login maka akan ada laporan ke log.txt kalau user tersebut telah berhasil login <br>
Dibawah ini contoh dari penerapan password mulai dari yang salah hingga yang benar : <br>
halo123 <br>
halo1234 <br>
halo <br>
HALO1234 <br>
Halo1234 <br>
![Output result](img/1c4.jpg) <br>
![Output result](img/1c5.jpg)


## 1d opsi setelah login
setelah user berhasil melakukan login akan muncul 2 opsi, yaitu : <br>
1.mendownload gambar, dan <br>
2.hitung jumlah login yang gagal dan berhasil

```shell
  folder=$(date +'%Y-%m-%d')_$username
```
- variabel folder berisikan tahun,bulan,tanggal hari_username yang sedang login. Variabel ini dibuat untuk mempermudah penamaan folder dan file zip yang nantinya dibuat

i. pada opsi pertama, akan diminta untuk memasukkan berapa jumlah gambar yang ingin di download dari link yang telah diberikan.
gambar yang telah di download akan dimasukan ke sebuah folder yang bernama "tahun-bulan-tanggal hari_username", gambar yang telah di download pun namanya diganti menjadi PIC_xx dengan nomor yang berurutan sesuai dengan jumlah yang didownload.
setelah itu folder yang menyimpan gambar-gambar tersebut di zipkan dengan nama sesuai dengan foldernya dan juga diberi password dari user yang sedang login.
```shell
    elif ! [ -e ${folder}.zip ] ;then
    mkdir $folder

        for ((num=1; num<=sum; num=num+1))
        do  
            if [ $num -lt 10 ] ; then
                echo "PIC_0$num"
                wget https://loremflickr.com/320/240 -O PIC_0$num.jpeg
                mv -n PIC_0$num.jpeg ./$folder
            else
                echo "PIC_$num"
                wget https://loremflickr.com/320/240 -O PIC_$num.jpeg 
                mv -n PIC_0$num.jpeg ./$folder
            fi
        done
        
       zip -rP $password $folder.zip $folder 
        rm -r $folder
        fi 
```
- ``-o`` memiliki fungsi untuk me-rename file
- ``-n`` memiliki fungsi untuk menjalankan perintah tanpa meminta ijin
- ``-e`` memiliki fungsi untuk mencari file/directory yang ditunjuk
- ``zip`` memiliki fungsi untuk zip folder/file yang ditunjuk

- ``-r`` berfungsi untuk menzip file secara terstruktur
- ``-P`` untuk zip berfungsi untuk menambahkan password sesuai dengan yang diberikan
- ``mv`` memindahkan file


pertama-tama akan dilakukan pengecekan terlebih dahulu terkait ada atau tidaknya file zip yang bernama sama. apabila telah diketahui tidak ada file zip yang bernama sama, maka akan langsung dilanjutkan dengan pembuatan directory untuk menyimpan gambar. setelah itu akan masuk ke perulangan for yang akan mendownload gambarnya sesuai dengan jumlah yang diberikan. lalu masuk ke if else yang mana akan di cek terlebih dahulu apakah jumlah gambar yang di download lebih dari 10 atau tidak. jika kurang dari 10 maka nama gambarnya akan menjadi "PIC_0$num" jika lebih dari 10 maka 0nya yang ada didepan akan dihapus menjadi "PIC_$num".
setelah namanya diganti maka akan langsung dipindahken ke folder yang sudah tersedia, setelah itu folder tersebut akan di zip lalu akan dihapus foldernya
![Output result](img/1d1.jpg)

selanjutnya jika ada file zip yang namanya sama.
```shell
if [ -e ${folder}.zip ] ;then
     unzip -P $password $folder.zip 

             for ((num=1; num<=sum; num=num+1))
        do  
            if [ $num -lt 10 ] ; then
                echo "PIC_0$num"
                wget https://loremflickr.com/320/240 -O PIC_S0$num.jpeg
                mv -n PIC_S0$num.jpeg ./$folder
            else
                echo "PIC_$num"
                wget https://loremflickr.com/320/240 -O PIC_S$num.jpeg 
                mv -n PIC_S$num.jpeg ./$folder
            fi
        done
        zip -rP $password $folder.zip $folder 
        rm -r $folder
```

sama seperti sebelumnya, awalnya akan dicek terlebih dahulu apakah ada zip yang bernama sama, jika ada maka akan di unzip il zip tersebut lalu akan masuk ke perulangan for untuk mendownload gambarnya.
bedanya nama gambar tersebut ditambahkan huruf "S" di depan urutan gambarnya, hal tersebut untuk mencegah apa bila ada file yang sama dan diminta untuk direwrite/replace
setelah itu akan di zip foldernya lalu di hapus folder tersebut 
![Output result](img/1d2.jpg)
ii. pada opsi ke dua diminta untuk menghitung jumlah login yang berhasil dan yang gagal
```shell
 elif [ $option -eq 2 ] ; then
    printf "Total login yang berhasil: " 
    grep "LOGIN: INFO User $username logged in" log.txt | wc -l
        printf "\nTotal login yang tidak berhasil: "  
        grep "LOGIN: ERROR Failed login attempt on user $username" log.txt | wc -l
```
 - `wc -l` Word Count berfungsi untuk menghitung kata/kalimat yang muncul pada sebuah file, -l untuk menghitung jumlah line. keduanya dikombinasikan 

 fungsi grep digunakan untuk mencari laporan yang berhasil dan gagal login dengan username yang sama dengan yang sedang login pada file log.txt
 ![Output result](img/1d3.jpg)




## Nomor 2
Pada soal nomor 2, terdapat file *log_website_daffainfo.log* yang diberikan untuk diolah dengan menggunakan script awk, dengan ketentuan:
<ol type="a">
    <li>Memuat folder bernama <i>forensic_log_website_daffainfo_log</i> </li>
    <li>Mencari <b>rata-rata request per jam</b> lalu memasukkan output-nya ke dalam file bernama <i>ratarata.txt</i> ke dalam folder 2a. </li>
    <li>Menampilkan <b>IP yang paling banyak</b> melakukan request ke server beserta <b>jumlah requestnya</b>. Lalu, memasukkan output-nya ke dalam file baru bernama <i>result.txt</i> ke dalam folder 2a. </li>
    <li>Mencari request yang menggunakan user-agent <b>curl</b>. Lalu, memasukkan output-nya ke dalam file <i>result.txt`</i>.</li>
    <li>Mencari IP Address yang menyerang pada <b>tanggal 22 Januari pukul 2 pagi</b>. Kemudian masukkan daftar IP tersebut kedalam file bernama <i>result.txt</i>.</li>
</ol>

Format File:
> **File ratarata.txt** <br>
> Rata-rata serangan adalah sebanyak *rata_rata* requests per jam
> <br>
> **File result.txt** <br>  
> IP yang paling banyak mengakses server adalah: *ip_address* sebanyak *jumlah_request* request.
>
> Ada *jumlah_req_curl* requests yang menggunakan curl sebagai user-agent
>
> *ip_address 1* <br>
> *ip_address 2* <br>
> *dst*

---
## Penyelesaian Nomor 2
Script dimulai dengan 
```sh
#!/bin/bash
``` 
syntax tersebut digunakan untuk memberi tahu kernel linux untuk mengeksekusi path yang disertakan dalam program (bash).

### 2a Pembuatan folder
Untuk penyelesaian pada soal nomor 2a, kelompok kami menggunakan syntax berikut:
```sh
mkdir -p forensic_log_website_daffainfo_log
```
- `mkdir` atau *make directory* merupakan syntax untuk pembuatan folder/directory, sedangkan `-p` merupakan kependekan dari `--parents` yang berfungsi untuk agar tidak terjadi error jika folder tersebut sudah ada
- `forensic_log_website_daffainfo_log` merupakan nama folder yang diminta pada soal

Berikut adalah gambar ketika menjalankan command `mkdir -p` pada terminal linux:
![mkdir -p](img/mkdir%20-p.png)

### 2b Rata-rata request per jam
Untuk mendapatkan rata-rata request per jam, cara yang kami gunakan adalah membagi total data dengan perbedaan jam. Script yang kami gunakan:

```sh
awk ' END{print "Rata-rata serangan adalah sebanyak", (NR-1)/12, "requests per jam"}
' log_website_daffainfo.log > forensic_log_website_daffainfo_log/ratarata.txt
```

- Script awk tersebut berfungsi untuk langsung melakukan pencetakan tulisan sesuai yang diminta pada soal. 
- `NR-1` merupakan total data yang mengakses website daffainfo. <br>`NR` menandakan *number of records* atau jumlah baris yang ada pada file log. NR dikurangi sebanyak 1 karena NR pertama hanya berisi nama setiap kolom.
- Perbedaan jam sebanyak `12` sebagai pembagi untuk mencari nilai rata-rata didapatkan secara manual dengan penjelasan berikut:

File *log_website_daffainfo.log*
>**Data pertama:** (Baris kedua) <br>
>"45.146.165.37":"22/Jan/2022:00:11:10":"POST /cgi-bin/.%2e/.%2e/.%2e/.%2e/bin/sh HTTP/1.1":400:5633:"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36"
>
>**Data terakhir:** <br>
> "45.146.165.37":"22/Jan/2022:12:00:00":"POST /vendor/phpunit/phpunit/src/Util/PHP/eval-stdin.php HTTP/1.1":404:5601:"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36"

Website daffainfo pertama kali diakses pada tanggal 22 Januari pukul 00:11:10, sedangkan website tersebut terakhir kali diakses pada tanggal 22 Januari pukul 12:00:00. Oleh karena itu, terdapat perbedaan sebanyak **12 jam**  dari pengaksesan pertama kali hingga pengaksesan terakhir.

- Hasil pembagian `NR-1` dengan `12` nantinya akan dimasukkan ke dalam file `ratarata.txt` yang berada pada folder **forensic_log_website_daffainfo_log**

### 2c IP yang paling banyak muncul
Untuk mencari IP yang paling banyak muncul, kami menggunakan script berikut:
```sh
awk 'BEGIN{FS = ":"}
    {if (count[$1]++ >= max) max = count[$1]}
    END{
        for(i in count)
            if (max == count[i]) 
                print "IP yang paling banyak mengakses server adalah:", 
                    substr(i, 2, length(i) - 2), "sebanyak", count[i], "request."
    }
' log_website_daffainfo.log > forensic_log_website_daffainfo_log/result.txt
```
- Script awk dimulai dengan `BEGIN{FS = ":"}`, yang artinya sebelum loop pada file dimulai, ditentukan terlebih dahulu `FS` atau *Field separator*-nya sebagai `:`

Contoh penggunaan FS, pada file *log_website_daffainfo.log*
> **NR ke-2** <br>
> "45.146.165.37":"22/Jan/2022:00:11:10":"POST /cgi-bin/.%2e/.%2e/.%2e/.%2e/bin/sh HTTP/1.1":400:5633:"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36"
>
>Hal ini berarti `$1` = "45.146.165.37" *atau* IP Address, `$2` = "22/Jan/2022 *atau* tanggal, dst.

- Pada bagian `if (count[$1]++ >= max) max = count[$1]`, kami membuat variabel array bernama `count` dengan array key kolom 1 atau IP, serta variabel `max` yang akan menampung jumlah terbesar IP yang sering muncul. Setiap awk melakukan perulangan pada setiap data dan menemukan IP yang sama, maka nilai array akan bertambah. Jika awk menemukan `count` dari array IP yang berbeda dan memiliki jumlah yang sama atau lebih besar dengan variabel `max`, maka nilai `max` akan berubah menjadi nilai `count` tersebut.
- Saat loop berakhir, terdapat `for(i in count)` yang berartikan loop untuk setiap array `i` atau setiap IP dari variabel count. Di dalam loop ini, terdapat `if (max == count[i])` yang berarti, ketika awk menemukan nilai max dari setiap array, maka awk akan menjalankan script berikut:

```sh
print "IP yang paling banyak mengakses server adalah:", substr(i, 2, length(i) - 2), "sebanyak", count[i], "request."
```

- Penjelasan untuk `substr(i, 2, length(i) - 2)`:
    - `substr` sendiri adalah salah satu command yang berfungsi untuk  memilih sebuah substring dari input. `substr` pada script tersebut ditujukan untuk menghilangkan tanda petik 2 yang terletak pada kolom pertama atau yang dilambangkan sebagai **i**. 
    - `substr` memiliki syntax `substr(s, a, b)` yang berarti mengembalikan sebanyak **b** huruf dari string **s**, dimulai pada posisi/huruf ke-**a**. 
    - pada bagian `length(i) - 2`, `length(i)` digunakan untuk mengembalikan panjang dari **i** (karena panjang dari setiap IP berbeda-beda), lalu `- 2` karena terdapat 2 petik yang ada pada **i** 

> Sebagai contoh, ketika i = *"216.93.144.47"* lalu dilakukan `substr(i, 2, length(i) - 2)`, akan mengembalikan tulisan *216.93.144.47* 

- Seluruh hasil script awk tersebut akan dimasukkan ke dalam file `result.txt` (baris pertama) di dalam folder *forensic_log_website_daffainfo_log*

### 2d Request oleh user-agent Curl
Untuk mencari jumlah request yang dilakukan oleh user-agent Curl, kami menggunakan script berikut:
```sh
awk 'BEGIN{FS=":"}
    /curl/ {count++} 
    END{print "\nAda", count, "requests yang menggunakan curl sebagai user agent"}
' log_website_daffainfo.log >> forensic_log_website_daffainfo_log/result.txt
```
- Pertama-tama, dideklarasikan terlebih dahulu bahwa `FS = ":"`
- Lalu, untuk mencari user agent bernama curl, menggunakan syntax  `/curl/ {count++}` yang berarti setiap baris yang mengandung "curl", variabel count akan diiterasikan sebanyak 1.
- Selanjutnya, pada saat loop berakhir, akan mencetak tulisan sesuai dengan format dan dimasukkan ke dalam file `result.txt` (baris terakhir) yang berada pada folder *forensic_log_website_daffainfo_log*

### 2e IP pada tanggal 22 Januari jam 2 pagi
Untuk mencari IP yang melakukan request pada tanggal 22 Januari jam 2 pagi, kami menggunakan script berikut:
```sh
awk 'BEGIN{FS=":"}
    {if($3 == "02" && substr($2,2,2) == "22")
    print "\n"substr($1, 2, length($1)-2)}
' log_website_daffainfo.log >> forensic_log_website_daffainfo_log/result.txt
```
- Proses awk dengan membaca file log_website_daffainfo.log
- `BEGIN{FS=":"}` berarti memberi tahu bahwa *field separator* dari file log_website_daffainfo.log adalah `:`. 
- `{if($3 == "02" && substr($2,2,2) == "22")` berarti apabila kolom ketiga atau **jam = 02** dan hasil substr dari kolom kedua atau **tanggal = 22**, maka akan melakukan `print substr($1, 2, length($1) - 2)`, yang tujuannya untuk mencetak sekaligus menghilangkan tanda petik 2 dari IP yang ada.
- Hasil awk tadi akan dimasukkan ke dalam file `result.txt` (baris terakhir) yang berada pada folder *forensic_log_daffainfo_log*

#### Output Nomor 2
Berikut adalah screenshot output dari file **ratarata.txt** sebagai penyelesaian dari nomor 2b:
![Output result](img/ratarata.png)
<br>

Berikut adalah screenshot output dari file **result.txt** sebagai penyelesaian nomor 2c hingga 2e:
![Output result](img/result.png)

## Nomor 3
Buatlah program monitoring resource pada komputer kalian. Cukup monitoring ram dan monitoring size suatu directory. Untuk ram gunakan command `free -m`. Untuk disk gunakan command `du -sh <target_path>`. Catat semua metrics yang didapatkan dari hasil `free -m`. Untuk hasil `du -sh <target_path>` catat size dari path directory tersebut. Untuk target_path yang akan dimonitor adalah `/home/{user}/`.
<ol type = "a">
    <li>a.	Masukkan semua metrics ke dalam suatu file log bernama <i>metrics_{YmdHms}.log</i>. {YmdHms} adalah waktu disaat file script bash kalian dijalankan. </li> 
    <li>b.	Script untuk mencatat metrics diatas diharapkan dapat berjalan otomatis pada <b>setiap menit</b>.</li>
    <li>c.	Kemudian, buat satu script untuk membuat agregasi file log ke satuan jam. Script agregasi akan memiliki info dari file-file yang tergenerate tiap menit. Dalam hasil file agregasi tersebut, terdapat nilai <b>minimum, maximum, dan rata-rata</b> dari tiap-tiap metrics. File agregasi akan ditrigger untuk dijalankan <b>setiap jam</b> secara otomatis. 
    Format nama file: <i>metrics_agg_{YmdH}.log</i> </li>
    <li>d.	Karena file log bersifat sensitif pastikan semua file log hanya dapat dibaca oleh user pemilik file.</li>
</ol>

Contoh isi file
> File **metrics_{YmdHMS}.log**
> mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size
> 15949,10067,308,588,5573,4974,2047,43,2004,/home/youruser/test/,74M

## Penyelesaian Nomor 3
### 3a Masukkan metrics ke file log
Pertama-tama, kita diminta terlebih dahulu command `free -m` saat dijalankan pada terminal, lalu memperoleh hasil sebagai berikut: 
![output perintah free -m.txt](img/perintah%20free%20-m.png.png) 

sedangkan untuk command du -sh akan mengeluarkan hasil.
![output perintah du -sh.txt](img/perintah%20du%20-sh.png.png)

Kemudian setelah kami mendapatkan data memori ram dari command yang sudah didapatkan, maka kami menggunakan konsep awk untuk mengolah data tersebut menjadi perbaris seperti di contoh yang ada pada soal. Berikut adalah script `minute_log.sh` yang kami gunakan untuk menyelesaikan soal tersebut:
```sh
#!/bin/bash
output="/home/razel/log/metrics_$(date +"%Y%m%d%H%M%S").log"

echo "type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" > $output

mem="$(free -m | awk '/Mem:/ {printf "%s,%s,%s,%s,%s,%s",$2,$3,$4,$5,$6,$7}')"
swap="$(free -m | awk '/Swap:/ {printf "%s,%s,%s",$2,$3,$4}')"
path="$(du -sh /home/razel | awk '{printf "%s,%s",$2,$1}')"
echo "$mem,$swap,$path" >> $output

chmod 700 $output
```
* Pertama-tama, setelah menggunakan `#!/bin/bash`, kami mendeklarasikan sebuah variabel bernama `output` yang berfungsi untuk menyimpan dimanakah letak outputnya. Output berada pada `/home/razel/log/metrics_$(date +"%Y%m%d%H%M%S").log`. <br> `/home/razel/log` merupakan directory dimana file metrics akan disimpan. Sedangkan `metrics_$(date + "%Y%m%d%H%M%S")` merupakan nama filenya yang terdiri atas tulisan `metrics_` dan dilanjutkan dengan variabel-variabel date. 

Berikut adalah penjelasan untuk variabel date:
| Command | Fungsi |
| :---: | --- |
| `date` | menampilkan tanggal saat program dijalankan |
| `%Y` | menampilkan tahun secara lengkap |
| `%m` | menampilkan bulan (01 - 12) |
| `%d` | menampilkan hari (01 - 31) |
| `%H` | menampilkan jam (00 - 23) |
| `%M` | menampilkan menit (00 - 59) |
| `%S` | menampilkan detik (00 - 59) |

> Contoh pada tanggal 28 Februari 2022, jam 23:01:49 ketika dijalankan script tersebut, akan muncul `metrics_20220228230149`
* Lalu, kamu menggunakan echo untuk menuliskan `mem_total` dan seterusnya, lalu dimasukkan ke dalam file metrics yang tersimpan dalam variabel `output`.
* Selanjutnya, kami menggunakan script berikut yang digunakan untuk mengambil data-data dari command `free -m` dan `du -sh home/user` dengan menggunakan awk. 
```sh
mem="$(free -m | awk '/Mem:/ {printf "%s,%s,%s,%s,%s,%s",$2,$3,$4,$5,$6,$7}')"
swap="$(free -m | awk '/Swap:/ {printf "%s,%s,%s",$2,$3,$4}')"
path="$(du -sh /home/razel | awk '{printf "%s,%s",$2,$1}')"
``` 
> Untuk path dilakukan print `$2, $1` karena saat melakukan command `du -sh`, akan keluar size di `$1` dan path di `$2`. Untuk menyesuaikan dengan format yang diberikan dari soal (path, path_size), maka hasil pengolahan di awk dibalik menjadi `$2, $1`.

* Lalu data tersebut akan disatukan dengan menggunakan command `echo($mem,$swap,$path)` dan hasilnya akan diletakkan di variabel `output` yang menyimpan dimana file metrics berada.
* Script diakhiri dengan `chmod 700 $output` (akan dijelaskan pada [poin 3d](#3d-file-log-hanya-bisa-diakses-oleh-user))

Hasil Screenshot Output pada folder log (pada tanggal 6 Maret 2022 pukul 15:06:01):
![hasil 3a](img/hasil%203a.png)

### 3b Metrics berjalan otomatis setiap menit
Untuk membuat script tersebut berjalan secara otomatis tiap menit,kami menggunakan cron untuk pelaksanaannya. Pertama-tama kami melakukan command `crontab -e` untuk membuat cron job baru. Kemudian kami memasukkan `* * * * * /bin/bash /home/razel/soal-sift-sisop-modul-1-ita07-2022/soal3/minute_log.sh`. Pada kolom pertama, terdiri atas 5 bintang yang berurutan yang memiliki arti script akan dijalankan setiap menit, sedangkan di kanannya bintang adalah letak script yang akan dilakukan cron job.

Hasil Screenshot Cronjob pada 3b (user = chery): 
![cronjob 3b](img/cronjob%203b.png)
<br>
Hasil Screenshot Cronjob setelah dibiarkan selama beberapa menit:
![hasil cronjob 3b](img/cronjob-menit.png) 

### 3c Membuat agregasi file log ke satuan jam
Script terletak pada `aggregate_minutes_to_hourly_log.sh`. Script 
```sh
#!/bin/bash
output="/home/razel/log/metrics_$(date +"%Y%m%d%H").log"
echo "type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" > $output
```
Pada script diatas kami mendeklarasikan variabel `output` yang digunakan untuk menyimpan path dimana letak file output akan diletakkan. File akan diberi nama `metrics_{YmdHms}.log` dan diletakkan pada `/home/razel/log/`. Untuk penjelasan nama file metrics sama seperti [penjelasan pada 3a](3a-masukkan-metrics-ke-file-log). 

Selanjutnya, untuk mendapatkan data-data yang ada pada setiap menit, kami menggunakan function `list` dengan command sebagai berikut:
```sh
function list(){
    for file in $(ls /home/razel/log/metrics_* | grep -v agg | grep $(date +"%Y%m%d%H"));
    do cat $file | grep -v mem;
    done
}
```
* Function `list` berisikan  `for` loop dari command berikut:
    * Command `ls /home/razel/log/metrics_*` bertujuan untuk melakukan `listing` terhadap directory `/home/razel/log/` dengan file yang berawalan `metrics_`
    * Command `grep -v agg` untuk memfilter seluruh file yang bernama `agg` untuk tidak diikutkan dalam `for` loop
    * Command `grep $(date + "%Y%m%d%H")` digunakan untuk memfilter file-file tersebut berdasarkan jam.
* Command `for` loop berisi `do cat $file | grep -v mem` yang berartikan, setiap file yang telah difilter dalam command `for` akan diprint dengan command `cat`, sedangkan `grep -v mem` berartikan print seluruh baris yang **tidak** mengandung string "mem".

Selanjutnya, untuk memproses data dari function `list`, kami membuat fuction baru bernama `mt_sort` yang berfungsi untuk mencari nilai minimum, maximum, dan nilai rata-rata dengan memanfaatkan awk seperti yang tertera pada script berikut: 
```sh
mt_sort=$(list | awk -F, '{
    if(min==""){
        min=max=$1
    };
    if($1>max){
        max=$1
    };
    if($1<max){
        min=$1
    };
    sum+=$1; n+=1
    } END {
        print min,max,sum/n
    }')
```
- Awk dimulai dengan `-F,` atau bentuk lainnya `FS = ","`. Selanjutnya ketika nilai dari variabel `min` tidak ada, maka nilai min dan max akan dimasukkan nilai dari `$1`. 
- Untuk command `if($1 > max)` dan `if($1 < max)` digunakan untuk mencari nilai maksimum dan minimum dari setiap data yang telah diproses pada AWK. 
- Variabel `sum` berartikan total dari penjumlahan setiap kolom `$1`, sedangkan `n` berartikan jumlah data yang diproses.
- Lalu setelah selesai melakukan loop di setiap data yang ada, maka `print min, max, sum/n` atau nilai minimal, maksimal, dan rata-rata.

Untuk mengambil data-data sesuai `mem_total`, hanya perlu memanggil function `mt_sort` dan memasukkan argumen yang sesuai. Lalu, agar dapat menyimpan hasil dari mt_sort, dilakukan dengan script berikut:

```sh
mt_min=$(echo $mt_sort | awk '{print $1}' | tr ',' '.')
mt_max=$(echo $mt_sort | awk '{print $2}' | tr ',' '.')
mt_avg=$(echo $mt_sort | awk '{print $3}' | tr ',' '.')
```

- Untuk nilai minimal dari mem_total akan disimpan pada `mt_min`, dan berlaku hal yang sama untuk nilai max dan rata-rata.
- Lalu, kami menggunakan command `tr` untuk mengubah string yang mengandung **","** menjadi **"."**

Untuk data-data selanjutnya seperti mem_used, mem_free, dan yang lainnya, akan digunakan cara yang sama seperti mem_total, dengan script sebagai berikut:
```sh
mu_sort=$(list | awk -F, '{
    if(min==""){
        min=max=$2
    };
    if($2>max){
        max=$2
    };
    if($2<max){
        min=$2
    };
    sum+=$2; n+=1
    } END {
        print min,max,sum/n
    }')
mu_min=$(echo $mu_sort | awk '{print $1}' | tr ',' '.')
mu_max=$(echo $mu_sort | awk '{print $2}' | tr ',' '.')
mu_avg=$(echo $mu_sort | awk '{print $3}' | tr ',' '.')

mf_sort=$(list | awk -F, '{
    if(min==""){
        min=max=$3
    };
    if($3>max){
        max=$3
    };
    if($3<max){
        min=$3
    };
    sum+=$3; n+=1
    } END {
        print min,max,sum/n
    }')
mf_min=$(echo $mf_sort | awk '{print $1}' | tr ',' '.')
mf_max=$(echo $mf_sort | awk '{print $2}' | tr ',' '.')
mf_avg=$(echo $mf_sort | awk '{print $3}' | tr ',' '.')

ms_sort=$(list | awk -F, '{
    if(min==""){
        min=max=$4
    };
    if($4>max){
        max=$4
    };
    if($4<max){
        min=$4
    };
    sum+=$4; n+=1
    } END {
        print min,max,sum/n
    }')
ms_min=$(echo $ms_sort | awk '{print $1}' | tr ',' '.')
ms_max=$(echo $ms_sort | awk '{print $2}' | tr ',' '.')
ms_avg=$(echo $ms_sort | awk '{print $3}' | tr ',' '.')

mb_sort=$(list | awk -F, '{
    if(min==""){
        min=max=$5
    };
    if($5>max){
        max=$5
    };
    if($5<max){
        min=$5
    };
    sum+=$5; n+=1
    } END {
        print min,max,sum/n
    }')
mb_min=$(echo $mb_sort | awk '{print $1}' | tr ',' '.')
mb_max=$(echo $mb_sort | awk '{print $2}' | tr ',' '.')
mb_avg=$(echo $mb_sort | awk '{print $3}' | tr ',' '.')

ma_sort=$(list | awk -F, '{
    if(min==""){
        min=max=$6
    };
    if($6>max){
        max=$6
    };
    if($6<max){
        min=$6
    };
    sum+=$6; n+=1
    } END {
        print min,max,sum/n
    }')
ma_min=$(echo $ma_sort | awk '{print $1}' | tr ',' '.')
ma_max=$(echo $ma_sort | awk '{print $2}' | tr ',' '.')
ma_avg=$(echo $ma_sort | awk '{print $3}' | tr ',' '.')

st_sort=$(list | awk -F, '{
    if(min==""){
        min=max=$7
    };
    if($7>max){
        max=$7
    };
    if($7<max){
        min=$7
    };
    sum+=$7; n+=1
    } END {
        print min,max,sum/n
    }')
st_min=$(echo $st_sort | awk '{print $1}' | tr ',' '.')
st_max=$(echo $st_sort | awk '{print $2}' | tr ',' '.')
st_avg=$(echo $st_sort | awk '{print $3}' | tr ',' '.')

su_sort=$(list | awk -F, '{
    if(min==""){
        min=max=$8
    };
    if($8>max){
        max=$8
    };
    if($8<max){
        min=$8
    };
    sum+=$8; n+=1
    } END {
        print min,max,sum/n
    }')
su_min=$(echo $su_sort | awk '{print $1}' | tr ',' '.')
su_max=$(echo $su_sort | awk '{print $2}' | tr ',' '.')
su_avg=$(echo $su_sort | awk '{print $3}' | tr ',' '.')

sf_sort=$(list | awk -F, '{
    if(min==""){
        min=max=$9
    };
    if($9>max){
        max=$9
    };
    if($9<max){
        min=$9
    };
    sum+=$9; n+=1
    } END {
        print min,max,sum/n
    }')
sf_min=$(echo $sf_sort | awk '{print $1}' | tr ',' '.')
sf_max=$(echo $sf_sort | awk '{print $2}' | tr ',' '.')
sf_avg=$(echo $sf_sort | awk '{print $3}' | tr ',' '.')

ps_sort=$(list | awk -F, '{
    if(min==""){
        min=max=$11
    };
    if($11>max){
        max=$11
    };
    if($11<max){
        min=$11
    };
    sum+=$11; n+=1
    } END {
        print min,max,sum/n
    }')
ps_min=$(echo $ps_sort | awk '{print $1}' | tr ',' '.')
ps_max=$(echo $ps_sort | awk '{print $2}' | tr ',' '.')
ps_avg=$(echo $ps_sort | awk '{print $3}' | tr ',' '.')
```

Selanjutnya, untuk melakukan pencetakan sesuai yang diminta pada soal, kami menggunakan script berikut:
```sh
path="/home/razel"

echo "minimum,$mt_min,$mu_min,$mf_min,$ms_min,$mb_min,$ma_min,$st_min,$su_min,$sf_min,$path,$ps_min" >> $output
echo "maximum,$mt_max,$mu_max,$mf_max,$ms_max,$mb_max,$ma_max,$st_max,$su_max,$sf_max,$path,$ps_max" >> $output
echo "average,$mt_avg,$mu_avg,$mf_avg,$ms_avg,$mb_avg,$ma_avg,$st_avg,$su_avg,$sf_avg,$path,$ps_avg" >> $output

chmod 700 $output
``` 
- Menggunakan command `echo` untuk melakukan pencetakan. Hasil dari echo akan dimasukkan ke dalam variabel `output` yang telah menyimpan dimana lokasi file akan disimpan
- Untuk command `chmod 700 $output`akan dijelaskan pada [poin 3d](#3d-file-log-hanya-bisa-diakses-oleh-user).
- Agar seluruh command dapat dilakukan secara otomatis selama setiap jam, dilakukan cron dengan command `crontab -e` lalu ke paling bawah dan memasukkan `59 * * * * /bin/bash /home/razel/Documents/sisop/soal-shift-sisop-modul-1-ita07-2022/soal3/aggregate_minutes_to_hourly_log.sh` yang memiliki arti, script akan dijalankan secara otomatis setiap jam, pada menit ke-59.

Screenshot cronjob yang digunakan untuk mengotomatisasi script nomor 3c: (user = chery)
![cronjob 3c](img/crontab%203c.png)
<br>
Screenshot script 3c yang dijalankan dengan menggunakan bash secara manual: (user = chery)
![output 3c](img/screenshot%203c.png)

### 3d File log hanya bisa diakses oleh user
Untuk kedua script (3a dan 3c) diakhiri dengan command berikut:
```sh
chmod 700 $output
```
- Command `chmod` digunakan untuk memberikan izin/akses kepada user tertentu (membaca, mengganti, dll).
- Setiap digit dari `700` berartikan:
    - `7` berartikan owner diperbolehkan untuk membaca, menulis dan mengeksekusi
    - `0` pertama dan kedua berartikan seluruh user (selain owner) tidak diperbolehkan untuk mengakses apapun di dalam directory output
- `$output` berartikan letak dimana pemberian akses dilakukan

Berikut adalah screenshot output dari command `ls -la` dari folder log yang telah dibuat untuk menunjukkan permission dari folder tersebut:
![ls -la](img/ls -la 3d.png)

### Kendala
Kendala kami adalah kesulitan untuk mengelola data dari command yang disediakan dari soal, yaitu free -m dan du -sh {target path} dengan menggunakan awk. Lalu, terkendala pada nomor 3c dimana harus mencari nilai minimum, maksimum dan nilai rata-ratanya.