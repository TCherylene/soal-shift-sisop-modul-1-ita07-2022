#!/bin/bash
output="/home/razel/log/metrics_$(date +"%Y%m%d%H%M%S").log"
echo "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" > $output

mem="$(free -m | awk '/Mem:/ {printf "%s,%s,%s,%s,%s,%s",$2,$3,$4,$5,$6,$7}')"
swap="$(free -m | awk '/Swap:/ {printf "%s,%s,%s",$2,$3,$4}')"
path="$(du -sh /home/razel | awk '{printf "%s,%s",$2,$1}')"

echo "$mem,$swap,$path" >> $output
chmod 700 $output

# * * * * * /bin/bash /home/razel/Documents/sisop/soal-shift-sisop-modul-1-ita07-2022/soal3/minute_log.sh