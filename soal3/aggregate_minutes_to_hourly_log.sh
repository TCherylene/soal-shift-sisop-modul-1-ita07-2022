#!/bin/bash
output="/home/razel/log/metrics_$(date +"%Y%m%d%H").log"
echo "type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" > $output

function list(){
    for file in $(ls /home/razel/log/metrics_* | grep $(date +"%Y%m%d%H"));
    do cat $file | grep -v mem;
    done
}

mt_sort=$(list | awk -F, '{
    if(min==""){
        min=max=$1
    };
    if($1>max){
        max=$1
    };
    if($1<max){
        min=$1
    };
    sum+=$1; n+=1
    } END {
        print min,max,sum/n
    }')
mt_min=$(echo $mt_sort | awk '{print $1}' | tr ',' '.')
mt_max=$(echo $mt_sort | awk '{print $2}' | tr ',' '.')
mt_avg=$(echo $mt_sort | awk '{print $3}' | tr ',' '.')

mu_sort=$(list | awk -F, '{
    if(min==""){
        min=max=$2
    };
    if($2>max){
        max=$2
    };
    if($2<max){
        min=$2
    };
    sum+=$2; n+=1
    } END {
        print min,max,sum/n
    }')
mu_min=$(echo $mu_sort | awk '{print $1}' | tr ',' '.')
mu_max=$(echo $mu_sort | awk '{print $2}' | tr ',' '.')
mu_avg=$(echo $mu_sort | awk '{print $3}' | tr ',' '.')

mf_sort=$(list | awk -F, '{
    if(min==""){
        min=max=$3
    };
    if($3>max){
        max=$3
    };
    if($3<max){
        min=$3
    };
    sum+=$3; n+=1
    } END {
        print min,max,sum/n
    }')
mf_min=$(echo $mf_sort | awk '{print $1}' | tr ',' '.')
mf_max=$(echo $mf_sort | awk '{print $2}' | tr ',' '.')
mf_avg=$(echo $mf_sort | awk '{print $3}' | tr ',' '.')

ms_sort=$(list | awk -F, '{
    if(min==""){
        min=max=$4
    };
    if($4>max){
        max=$4
    };
    if($4<max){
        min=$4
    };
    sum+=$4; n+=1
    } END {
        print min,max,sum/n
    }')
ms_min=$(echo $ms_sort | awk '{print $1}' | tr ',' '.')
ms_max=$(echo $ms_sort | awk '{print $2}' | tr ',' '.')
ms_avg=$(echo $ms_sort | awk '{print $3}' | tr ',' '.')

mb_sort=$(list | awk -F, '{
    if(min==""){
        min=max=$5
    };
    if($5>max){
        max=$5
    };
    if($5<max){
        min=$5
    };
    sum+=$5; n+=1
    } END {
        print min,max,sum/n
    }')
mb_min=$(echo $mb_sort | awk '{print $1}' | tr ',' '.')
mb_max=$(echo $mb_sort | awk '{print $2}' | tr ',' '.')
mb_avg=$(echo $mb_sort | awk '{print $3}' | tr ',' '.')

ma_sort=$(list | awk -F, '{
    if(min==""){
        min=max=$6
    };
    if($6>max){
        max=$6
    };
    if($6<max){
        min=$6
    };
    sum+=$6; n+=1
    } END {
        print min,max,sum/n
    }')
ma_min=$(echo $ma_sort | awk '{print $1}' | tr ',' '.')
ma_max=$(echo $ma_sort | awk '{print $2}' | tr ',' '.')
ma_avg=$(echo $ma_sort | awk '{print $3}' | tr ',' '.')

st_sort=$(list | awk -F, '{
    if(min==""){
        min=max=$7
    };
    if($7>max){
        max=$7
    };
    if($7<max){
        min=$7
    };
    sum+=$7; n+=1
    } END {
        print min,max,sum/n
    }')
st_min=$(echo $st_sort | awk '{print $1}' | tr ',' '.')
st_max=$(echo $st_sort | awk '{print $2}' | tr ',' '.')
st_avg=$(echo $st_sort | awk '{print $3}' | tr ',' '.')

su_sort=$(list | awk -F, '{
    if(min==""){
        min=max=$8
    };
    if($8>max){
        max=$8
    };
    if($8<max){
        min=$8
    };
    sum+=$8; n+=1
    } END {
        print min,max,sum/n
    }')
su_min=$(echo $su_sort | awk '{print $1}' | tr ',' '.')
su_max=$(echo $su_sort | awk '{print $2}' | tr ',' '.')
su_avg=$(echo $su_sort | awk '{print $3}' | tr ',' '.')

sf_sort=$(list | awk -F, '{
    if(min==""){
        min=max=$9
    };
    if($9>max){
        max=$9
    };
    if($9<max){
        min=$9
    };
    sum+=$9; n+=1
    } END {
        print min,max,sum/n
    }')
sf_min=$(echo $sf_sort | awk '{print $1}' | tr ',' '.')
sf_max=$(echo $sf_sort | awk '{print $2}' | tr ',' '.')
sf_avg=$(echo $sf_sort | awk '{print $3}' | tr ',' '.')

ps_sort=$(list | awk -F, '{
    if(min==""){
        min=max=$11
    };
    if($11>max){
        max=$11
    };
    if($11<max){
        min=$11
    };
    sum+=$11; n+=1
    } END {
        print min,max,sum/n
    }')
ps_min=$(echo $ps_sort | awk '{print $1}' | tr ',' '.')
ps_max=$(echo $ps_sort | awk '{print $2}' | tr ',' '.')
ps_avg=$(echo $ps_sort | awk '{print $3}' | tr ',' '.')


path="/home/razel"

echo "minimum,$mt_min,$mu_min,$mf_min,$ms_min,$mb_min,$ma_min,$st_min,$su_min,$sf_min,$path,$ps_min" >> $output
echo "maximum,$mt_max,$mu_max,$mf_max,$ms_max,$mb_max,$ma_max,$st_max,$su_max,$sf_max,$path,$ps_max" >> $output
echo "average,$mt_avg,$mu_avg,$mf_avg,$ms_avg,$mb_avg,$ma_avg,$st_avg,$su_avg,$sf_avg,$path,$ps_avg" >> $output

chmod 700 $output

#59 * * * * /bin/bash /home/razel/Documents/sisop/soal-shift-sisop-modul-1-ita07-2022/soal3/aggregate_minutes_to_hourly_log.sh