#!/bin/bash
#!/bin/bash

if [ ! -d log.txt ]; then
    touch log.txt
fi

if [ ! -d ./users ]; then
    mkdir ./users
fi

if [ ! -d ./users/user.txt ]; then
    touch ./users/user.txt
fi

time="$(date +'%D %H:%M:%S')"

read -p "Masukkan username : " username
if grep -q "$username" ./users/user.txt ; then
   echo "User $username sudah ada"
   echo "$time REGISTER: ERROR $username already exists" >> log.txt 
   exit 1
fi

read -sp "Masukkan password : " password
if [ ${#password} -lt 8 ] ; then
   echo "Password harus lebih dari 8 karakter"
   exit 1
fi

if [ $password = ${password^^} ] ; then 
   echo "Password minimal 1 huruf kecil" 
   exit 1
elif [ $password = ${password,,} ] ; then 
   echo "Password minimal 1 huruf kapital"

   exit 1
fi

if ! [[ $password =~ [0-9] ]] ; then 
   echo "Password harus ada angkanya"
   exit 1
elif
[ $username == $password ]; then 
     echo "Password tidak boleh sama dengan username"
     exit 1
else
     echo "$username $password" >> ./users/user.txt && echo "$username berhasil ditambahkan" &&  echo "$time REGISTER: INFO User $username registered successfully" >> log.txt  
fi
   
